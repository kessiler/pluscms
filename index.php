<?php
/**
 * @author Kessiler Rodrigues && Marcos Silva
 */
require_once __DIR__ . '/vendor/autoload.php';
try {
    $app = new Silex\Application();
    require __DIR__ . '/src/settings.php';
    if(BLOCK_COUNTRY_ENABLED) {
        if(isset($_SERVER["HTTP_CF_IPCOUNTRY"]) && in_array($_SERVER['HTTP_CF_IPCOUNTRY'], $countryNames)) {
            throw new Exception;
        }
    }
    require __DIR__ . '/src/app.php';
    require __DIR__ . '/src/routes.php';
    $app->run();
} catch (PDOException $e) {
    echo "Ocorreu um erro ao tentar conectar-se ao banco de dados.";
} catch (Exception $e) {
    echo "Ocorreu um erro.";
}

