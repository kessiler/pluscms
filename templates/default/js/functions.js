function onlyNumbers(e) {
    if (document.all) {
        var evt = event.keyCode;
    }
    else {
        var evt = e.charCode;
    }
    var valid_chars = '0123456789';
    var chr = String.fromCharCode(evt);
    if (valid_chars.indexOf(chr) > -1) {
        return true;
    }
    if (valid_chars.indexOf(chr) > -1 || evt < 9) {
        return true;
    }
    if (valid_chars.indexOf(chr) > 30 || evt < 35) {
        return true;
    }
    return false;
}


$(document).ready(function () {
    var height = $(window).height();
    var contentHeight = $('#main-content').height();
    var footerHeight = $('.footer').height();
    var headerHeight = $('header').height();
    var diffHeight = height - (contentHeight + headerHeight + footerHeight) - 25;
    if (diffHeight > 0) {
        $('#main-content').css('min-height', contentHeight + diffHeight);
    }

    $('#slide').nivoSlider({
        effect: 'random',
        animSpeed: 500,
        pauseTime: 3000,
        startSlide: 0,
        controlNav: false,
        directionNav: true,
        pauseOnHover: true,
        prevText: 'Próximo',
        nextText: 'Anterior'
    });
});