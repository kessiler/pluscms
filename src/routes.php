<?php
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Kessiler Rodrigues && Marcos Silva
 */
$app->get('/', 'Mania\Controller\IndexController::indexAction')->bind('index');
$app->get('/logout', 'Mania\Controller\UserController::logoutAction')->bind('logout');
$app->get('/recuperar', 'Mania\Controller\UserController::recoverAction')->bind('recuperar');
$app->post('/recuperar', 'Mania\Controller\UserController::recoverAction');
$app->post('/recuperar/{hashKey}', 'Mania\Controller\UserController::recoverHashAction');
$app->match('/recuperar/{hashKey}', 'Mania\Controller\UserController::recoverHashAction');
$app->get('/cadastro', 'Mania\Controller\RegisterController::indexAction')->bind('cadastro');
$app->post('/cadastro', 'Mania\Controller\RegisterController::registerAction');
$app->post('/cadastro/{usuario}', 'Mania\Controller\RegisterController::registerAction');
$app->match('/cadastro/{usuario}', 'Mania\Controller\RegisterController::indexAction');
$app->get('/equipe', 'Mania\Controller\StaffController::indexAction')->bind('equipe');
$app->get('/noticias','Mania\Controller\NoticesController::indexAction')->bind('noticias');
$app->match('/noticias/{noticia}', 'Mania\Controller\NoticesController::viewAction')->bind('noticia');
$app->get('/ranking', 'Mania\Controller\RankingController::indexAction')->bind('ranking');
$app->get('/client', 'Mania\Controller\ClientController::indexAction')->bind('client');
$app->get('/user/', 'Mania\Controller\UserController::myPageAction')->bind('user');
$app->get('/user/config', 'Mania\Controller\UserController::configAction')->bind('config');
$app->get('/admin/noticias','Mania\Controller\AdminNoticesController::indexAction')->bind('adminNotices');
$app->match('/admin/noticias/add', 'Mania\Controller\AdminNoticesController::addAction')->bind('adminNoticesAdd');
$app->match('/admin/noticias/{notice}/edit', 'Mania\Controller\AdminNoticesController::editAction')->bind('adminNoticesEdit');
$app->match('/admin/noticias/{notice}/delete', 'Mania\Controller\AdminNoticesController::deleteAction')->bind('adminNoticesDelete');

$app->error(function (\Exception $e, $code) use ($app) {
    if ($app['debug']) {
        return;
    }
    switch ($code) {
        case 404:
            return $app['twig']->render('404.html.twig', array('errorMessage' => $e->getMessage()));
            break;
        default:
            $message = 'We are sorry, but something went terribly wrong.';
    }
    return new Response($message, $code);
});