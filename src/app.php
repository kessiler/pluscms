<?php
/**
 * @author Kessiler Rodrigues && Marcos Silva
 */
date_default_timezone_set('America/Sao_Paulo');
Symfony\Component\HttpFoundation\Request::enableHttpMethodParameterOverride();
if (DEBUGGER) {
    $app['debug'] = true;
}

$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
$app->register(new Silex\Provider\SessionServiceProvider());
$app->register(new Silex\Provider\SwiftmailerServiceProvider());
$app['swiftmailer.options'] = array(
    'host'       => STMP_HOST,
    'port'       => SMTP_PORT,
    'username'   => SMTP_MAIL,
    'password'   => PASSWORD_MAIL,
    'encryption' => SMTP_ENCRYPTION,
    'auth_mode' => SMTP_AUTH_MODE
);
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver' => 'pdo_mysql',
        'host' => HOST,
        'dbname' => DATABASE,
        'user' => USER,
        'port' => PORT,
        'password' => PASSWORD,
        'charset' => 'utf8',
    ),

));

$app->register(new Silex\Provider\SecurityServiceProvider(), array(
    'security.firewalls' => array(
        'default' => array(
            'pattern' => '^.*$',
            'anonymous' => true,
            'form' => array('login_path' => '/',
                            'check_path' => 'login_check',
                            'username_parameter' => 'username',
                            'password_parameter' => 'password'),
            'logout' => array('logout_path' => '/logout'),
            'users' => $app->share(function () use ($app) {
                    return new Mania\Repository\UserRepository($app['db'], $app['security.encoder.digest']);
            }),
        ),
    ),
    'security.role_hierarchy' => array(
        'ROLE_FOUNDER' => array('ROLE_ADMIN'),
        'ROLE_ADMIN' => array('ROLE_MANAGER'),
        'ROLE_MANAGER' => array('ROLE_USER'),
        'ROLE_USER' => array()
    ),
));

$app['security.encoder.digest'] = $app->share(function () {
    return new \Mania\Service\PasswordEncoder();
});

$app->boot();

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__ . '/../templates/'.TEMPLATE,
    'twig.options' => array(
        'cache' => TEMPLATE_CACHE ? __DIR__ . '/../cache' : false
    )
));

$app['repository.server'] = $app->share(function ($app) {
    return new Mania\Repository\ServerRepository($app['db']);
});

$app['repository.user'] = $app->share(function ($app) {
    return new Mania\Repository\UserRepository($app['db'], $app['security.encoder.digest']);
});

$app['repository.staff'] = $app->share(function ($app) {
    return new Mania\Repository\StaffRepository($app['db']);
});

$app['repository.ranking'] = $app->share(function ($app) {
    return new Mania\Repository\RankingRepository($app['db']);
});

$app['repository.notices'] = $app->share(function ($app) {
    return new Mania\Repository\NoticesRepository($app['db']);
});

$app['service.shoutcast'] = $app->share(function () {
    $shoutCast = new \Mania\Service\ShoutCast();
    $shoutCast->host = RADIO_HOST;
    $shoutCast->port = RADIO_PORT;
    $shoutCast->passwd = RADIO_PASSWORD;
    return $shoutCast;
});

$app->before(function (Symfony\Component\HttpFoundation\Request $request) use ($app) {

    $protected = array(
        '/client' => 'ROLE_USER',
        '/admin' => 'ROLE_MANAGER'
    );

    $path = $request->getPathInfo();
    foreach ($protected as $protectedPath => $role) {
        if (strpos($path, $protectedPath) !== FALSE && !$app['security']->isGranted($role)) {
            $redirect = $app['url_generator']->generate('index');
            return $app->redirect($redirect);
        }
    }
});


/* TEMPLATE GLOBALS */
$app['twig']->addGlobal('title', TITLE_SITE);
$app['twig']->addGlobal('footerText', html_entity_decode(FOOTER_TEXT));
$app['twig']->addGlobal('usersOnline', $app['repository.server']->getCountUsersOnline());
$app['twig']->addGlobal('blockEnterClient', BLOCK_ENTER_CLIENT);

return $app;