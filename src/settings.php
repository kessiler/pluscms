<?php
/**
 * @author Kessiler Rodrigues && Marcos Silva
 * DEBUGGER SETTINGS
 */
define('DEBUGGER', true);

/**
 * DATABASE SETTINGS
 */
define('HOST', '127.0.0.1');
define('PORT', 3306);
define('USER', 'root');
define('PASSWORD', '');
define('DATABASE', 'habbo');

/**
 * RADIO SETTINGS
 */
define('RADIO_HOST', '198.24.138.226');
define('RADIO_PORT', '9842');
define('RADIO_PASSWORD', '0B8R4SUyHMRSD');

/**
 * MAILER SETTINGS
 */
define('STMP_HOST', 'smtp.onmania.net');
define('SMTP_PORT', '587');
define('SMTP_MAIL', 'noreply@onmania.net');
define('PASSWORD_MAIL', 'Fh2Ssf631');
define('SMTP_ENCRYPTION', null);
define('SMTP_AUTH_MODE', null);

/**
 * TEMPLATE SETTINGS
 */
define('TEMPLATE', 'default');
define('TEMPLATE_CACHE', false);
define('TITLE_SITE', 'Mania Hotel - Seja Bem-Vindo!');
define('FOOTER_TEXT', 'Copyrights &copy; 2014 | Mania Hotel - Todos os direitos reservados.');

/**
 * RANKING SETTINGS
 */
define('TOP_RESPECTED', 5);
define('TOP_DIAMONDS', 5);
define('TOP_ADVISERS', 5);
define('TOP_ACHIEVEMENTS_POINTS', 5);

/**
 * BLOCK SETTINGS
 */
define('BLOCK_ENTER_CLIENT', 0);
define('BLOCK_LOGGED_RECORD', true);
define('BLOCK_COUNTRY_ENABLED', true);
$countryNames = array('CA', 'FI', 'US');


