<?php
/**
 * Created by PhpStorm.
 * User: KESSILER
 * Date: 12/07/14
 * Time: 21:53
 */

namespace Mania\Entity;

use Symfony\Component\Security\Core\User\AdvancedUserInterface;

/**
 * Class UserEntity
 * @author Kessiler Rodrigues && Marcos Silva
 * @package Mania\Entity
 */
class UserEntity implements AdvancedUserInterface {

    private $username;
    private $id;
    private $password;
    private $enabled;
    private $accountNonExpired;
    private $credentialsNonExpired;
    private $accountNonLocked;
    private $roles;
    private $credits;
    private $points;
    private $diamonds;
    private $references;
    private $look;
    private $motto;


    public function __construct($data, array $roles = array(), $enabled = true, $userNonExpired = true, $credentialsNonExpired = true, $userNonLocked = true)
    {
        if (!isset($data)) {
            throw new \InvalidArgumentException('The username cannot be empty.');
        }

        $this->id       = $data['id'];
        $this->username = $data['username'];
        $this->password = $data['password'];
        $this->credits = $data['credits'];
        $this->points = $data['activity_points'];
        $this->references = $data['referencias'];
        $this->diamonds = $data['seasonal_currency'];
        $this->look     = $data['look'];
        $this->motto     = $data['motto'];
        $this->enabled = $enabled;
        $this->accountNonExpired = $userNonExpired;
        $this->credentialsNonExpired = $credentialsNonExpired;
        $this->accountNonLocked = $userNonLocked;
        $this->roles = $roles;
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * {@inheritdoc}
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * {@inheritdoc}
     */
    public function getSalt()
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * {@inheritdoc}
     */
    public function isAccountNonExpired()
    {
        return $this->accountNonExpired;
    }

    /**
     * {@inheritdoc}
     */
    public function isAccountNonLocked()
    {
        return $this->accountNonLocked;
    }

    /**
     * {@inheritdoc}
     */
    public function isCredentialsNonExpired()
    {
        return $this->credentialsNonExpired;
    }

    /**
     * {@inheritdoc}
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
    }

    /**
     * @return mixed
     */
    public function getCredits()
    {
        return $this->credits;
    }

    /**
     * @return mixed
     */
    public function getDiamonds()
    {
        return $this->diamonds;
    }

    /**
     * @return mixed
     */
    public function getLook()
    {
        return $this->look;
    }

    /**
     * @return mixed
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @return mixed
     */
    public function getReferences()
    {
        return $this->references;
    }

    /**
     * @return mixed
     */
    public function getMotto()
    {
        return $this->motto;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

}