<?php
/**
 * Created by PhpStorm.
 * User: KESSILER
 * Date: 12/07/14
 * Time: 18:12
 */

namespace Mania\Repository;

use Doctrine\DBAL\Connection;

/**
 * Class RankingRepository
 * @author Kessiler Rodrigues && Marcos Silva
 * @package Mania\Repository
 */
class RankingRepository
{
    protected $db;

    const RESPECTED = 'respect';
    const DIAMONDS = 'seasonal_currency';
    const ADVISER = 'referencias';
    const ACHIEVEMENT_POINTS = 'achievement_score';

    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    public function getRanking($limit, $offset = 0, $type = RankingRepository::RESPECTED) {
        $queryBuilder = $this->db->createQueryBuilder();
        switch($type) {
            case self::RESPECTED:
            case self::ACHIEVEMENT_POINTS:
                $queryBuilder
                ->select('u.username, u.look, us.'.$type)
                ->from('user_stats', 'us')
                ->join('us', 'users', 'u', 'u.id = us.id')
                ->where('u.rank < 3')
                ->setMaxResults($limit)
                ->setFirstResult($offset)
                ->orderBy('us.' . $type, 'DESC');
                break;
            case self::ADVISER:
            case self::DIAMONDS:
            $queryBuilder
                ->select('u.username, u.look, u.'.$type)
                ->from('users', 'u')
                ->where('rank < 3')
                ->setMaxResults($limit)
                ->setFirstResult($offset)
                ->orderBy('u.' . $type, 'DESC');
                break;
            default: break;
        }
        $statement = $queryBuilder->execute();
        $rankings = $statement->fetchAll();
        return $rankings;
    }

}