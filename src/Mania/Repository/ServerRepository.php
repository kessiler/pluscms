<?php
/**
 * Created by PhpStorm.
 * User: KESSILER
 * Date: 08/07/14
 * Time: 01:22
 */

namespace Mania\Repository;

use Doctrine\DBAL\Connection;

/**
 * Class ServerRepository
 * @author Kessiler Rodrigues && Marcos Silva
 * @package Mania\Repository
 */
class ServerRepository
{
    protected $db;

    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    public function getCountUsersOnline() {
        return $this->db->fetchColumn('SELECT users_online FROM server_status');
    }
}


