<?php
/**
 * Created by PhpStorm.
 * User: KESSILER
 * Date: 08/07/14
 * Time: 12:57
 */

namespace Mania\Repository;

use Doctrine\DBAL\Connection;
use Mania\Entity\UserEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

/**
 * Class UserRepository
 * @author Kessiler Rodrigues && Marcos Silva
 * @package Mania\Repository
 */
class UserRepository implements UserProviderInterface
{

    protected $db;
    protected $encoder;

    public function __construct(Connection $db, $encoder)
    {
        $this->db = $db;
        $this->encoder = $encoder;
    }

    public function loadUserByUsername($username)
    {
        $stmt = $this->db->executeQuery('SELECT * FROM users WHERE username = ? or mail = ?', array($username, $username));
        if (!$user = $stmt->fetch()) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
        }

        $roles = array('ROLE_USER');
        switch($user['rank']) {
            case 5:
                $roles = array('ROLE_MANAGER'); break;
            case 6:
                $roles = array('ROLE_ADMIN'); break;
            case 7:
                $roles = array('ROLE_FOUNDER'); break;
            default: break;
        }
        return new UserEntity($user, $roles, true, true, true, true);
    }

    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', $class));
        }

        return $this->loadUserByUsername($user->getUsername());
    }


    public function supportsClass($class)
    {
        return $class === 'Mania\Entity\UserEntity';
    }

    public function getLastRegisteredUsers($limit = 3, $offset = 0) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('u.username, u.account_created, u.look')
            ->from('users', 'u')
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->orderBy('u.id', 'DESC');
        $data = $queryBuilder->execute();
        return $data->fetchAll();
    }

    public function find($id)
    {
        $data = $this->db->fetchAssoc('SELECT * FROM users WHERE id = ?', array($id));
        return $data;
    }

    public function getCountByIp($ip) {
        return $this->db->fetchColumn('SELECT COUNT(id) FROM users WHERE ip_reg = ? or ip_last = ?', array($ip, $ip));
    }

    public function findBy($column, $value) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder->select('u.*')
                     ->from('users', 'u')
                     ->where($column . ' = :'.$column)
                     ->setParameter(':'.$column, $value);
        $data = $queryBuilder->execute();
        return $data->fetchAll();
    }

    public function findByMailAndPin($mail, $pin) {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder->select('u.*')
            ->from('users', 'u')
            ->where('mail = :mail')
            ->andWhere('pin = :pin')
            ->setParameters(array(':mail' => $mail, 'pin' => $pin));
        $data = $queryBuilder->execute();
        return $data->fetchAll();
    }

    public function save($data) {
        if(isset($data['id'])) {
            return $this->db->update('users', $data, array('id' => $data['id']));
        } else {
            $this->db->insert('users', $data);
            return $this->db->lastInsertId();
        }

    }

} 