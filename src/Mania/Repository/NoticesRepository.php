<?php
/**
 * Created by PhpStorm.
 * User: KESSILER
 * Date: 08/07/14
 * Time: 01:22
 */

namespace Mania\Repository;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Query\Expression\ExpressionBuilder;

/**
 * Class NoticesRepository
 * @author Kessiler Rodrigues && Marcos Silva
 * @package Mania\Repository
 */
class NoticesRepository
{
    protected $db;

    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    public function findAll($limit, $offset = 0, $orderBy = array())
    {
        if (!$orderBy) {
            $orderBy = array('id' => 'DESC');
        }
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('n.id, n.title, n.longstory, n.shortstory, n.image, u.username as author')
            ->from('cms_news', 'n')
            ->innerJoin('n', 'users', 'u', 'n.author = u.id')
            ->setMaxResults($limit)
            ->setFirstResult($offset)
            ->orderBy('n.' . key($orderBy), current($orderBy));
        $statement = $queryBuilder->execute();
        $notices = $statement->fetchAll();
        return $notices;
    }

    public function find($id)
    {
        $data = $this->db->fetchAssoc('SELECT * FROM cms_news WHERE id = ?', array($id));
        return $data;
    }

    public function getCount()
    {
        return $this->db->fetchColumn('SELECT COUNT(id) FROM cms_news');
    }

    public function save($data)
    {
        if (isset($data['id'])) {
            return $this->db->update('cms_news', $data, array('id' => $data['id']));
        } else {
            $this->db->insert('cms_news', $data);
            return $this->db->lastInsertId();
        }
    }

    public function delete($id)
    {
        return $this->db->delete('cms_news', array('id' => $id));
    }
}


