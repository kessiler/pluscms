<?php
/**
 * Created by PhpStorm.
 * User: KESSILER
 * Date: 10/07/14
 * Time: 23:07
 */

namespace Mania\Repository;

use Doctrine\DBAL\Connection;

/**
 * Class StaffRepository
 * @author Kessiler Rodrigues && Marcos Silva
 * @package Mania\Repository
 */
class StaffRepository
{
    protected $db;
    const FOUNDER = 7;
    const ADMIN = 6;
    const MANAGER = 5;
    const MODERATOR = 4;
    const HELPER = 3;

    public function __construct(Connection $db)
    {
        $this->db = $db;
    }

    public function getStaffByType($type)
    {
        $queryBuilder = $this->db->createQueryBuilder();
        $queryBuilder
            ->select('u.*')
            ->from('users', 'u')
            ->where('rank = :type')
            ->setParameter(':type', $type)
            ->orderBy('u.username', 'ASC');
        $data = $queryBuilder->execute();
        return $data->fetchAll();
    }

}