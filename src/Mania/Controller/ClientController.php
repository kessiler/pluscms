<?php
/**
 * Created by PhpStorm.
 * User: KESSILER
 * Date: 13/07/14
 * Time: 02:45
 */

namespace Mania\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ClientController
 * @author Kessiler Rodrigues && Marcos Silva
 * @package Mania\Controller
 */
class ClientController
{

    public function indexAction(Request $request, Application $app)
    {
        $user = $app['security']->getToken()->getUser();
        $appToken = '8458iqejf98425t8huijef248f40';
        $sso = md5($user->getId() . sha1( $user->getId()) .$appToken);
        $date = new \DateTime();
        $data = array(
            'id' => $user->getId(),
            'ip_last' => $request->getClientIp(),
            'auth_ticket' => $sso,
            'last_online' => $date->getTimestamp());
        $app['repository.user']->save($data);
        $notices = $app['repository.notices']->findAll(10);

        $notice = '';
        if (!empty($notices)) {
            $notice = $notices[0];
        }

        $data = array('sso' => $sso,
            'userHash' => sha1($user->getId()),
            'othersNotices' => $notices,
            'notice' => $notice,
            'error' => $app['security.last_error']($request)
        );
        return $app['twig']->render('client.html.twig', $data);
    }
} 