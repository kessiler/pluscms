<?php
/**
 * Created by PhpStorm.
 * User: KESSILER
 * Date: 08/07/14
 * Time: 21:09
 */

namespace Mania\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class NoticesController
 * @author Kessiler Rodrigues && Marcos Silva
 * @package Mania\Controller
 */
class NoticesController {

    const LIMIT = 30;

    public function indexAction(Request $request, Application $app) {
        $notices = $app['repository.notices']->findAll(self::LIMIT);

        $notice = '';
        if(!empty($notices)) {
            $notice = $notices[0];
        }

        $data = array(
            'othersNotices' => $notices,
            'notice' => $notice,
            'error' => $app['security.last_error']($request)
        );
        return $app['twig']->render('noticias.html.twig', $data);
    }

    public function viewAction(Request $request, Application $app)
    {
        $notice = $request->attributes->get('noticia');
        if (!$notice) {
            $app->abort(404, 'Notícia não encontrada.');
        }

        $notice = $app['repository.notices']->find($notice);
        $notices = $app['repository.notices']->findAll(self::LIMIT);

        $data = array(
            'notice' => $notice,
            'othersNotices' => $notices,
            'error' => $app['security.last_error']($request)
        );
        return $app['twig']->render('noticias.html.twig', $data);
    }

    public function managerAction(Request $request, Application $app) {

    }

} 