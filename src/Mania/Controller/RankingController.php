<?php
/**
 * Created by PhpStorm.
 * User: KESSILER
 * Date: 12/07/14
 * Time: 04:12
 */

namespace Mania\Controller;

use Mania\Repository\RankingRepository;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RankingController
 * @author Kessiler Rodrigues && Marcos Silva
 * @package Mania\Controller
 */
class RankingController {

    public function indexAction(Request $request, Application $app)
    {
        $respected = $app['repository.ranking']->getRanking(TOP_RESPECTED, 0, RankingRepository::RESPECTED);
        $diamonds = $app['repository.ranking']->getRanking(TOP_DIAMONDS, 0, RankingRepository::DIAMONDS);
        $advisers = $app['repository.ranking']->getRanking(TOP_ADVISERS, 0, RankingRepository::ADVISER);
        $achievements = $app['repository.ranking']->getRanking(TOP_ACHIEVEMENTS_POINTS, 0, RankingRepository::ACHIEVEMENT_POINTS);
        $data = array('error' => $app['security.last_error']($request),
            'rankingRespected' => $respected,
            'rankingDiamonds' => $diamonds,
            'rankingAdvisers' => $advisers,
            'rankingAchievementPoints' => $achievements
        );
        return $app['twig']->render('ranking.html.twig', $data);
    }

} 