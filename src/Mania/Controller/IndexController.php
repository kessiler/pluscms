<?php
/**
 * Created by PhpStorm.
 * User: KESSILER
 * Date: 08/07/14
 * Time: 01:20
 */

namespace Mania\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class IndexController
 * @author Kessiler Rodrigues && Marcos Silva
 * @package Mania\Controller
 */
class IndexController
{
    public function indexAction(Request $request, Application $app)
    {
        $users = $app['repository.user']->getLastRegisteredUsers();
        $notices = $app['repository.notices']->findAll(5);
        $data = $app['repository.user']->findBy('username', 'iPlezier');
        if($data) {
            $radio = array('user' => $data[0]['username'], 'look' => $data[0]['look'], 'program' => '', 'dj' => '');    
        } else {
            $radio = array('user' => '', 'look' => '', 'program' => '', 'dj' => '');
        }
        if ($app['service.shoutcast']->openstats()) {
            if ($app['service.shoutcast']->GetStreamStatus()) {
                $radio['program'] = $app['service.shoutcast']->GetServerGenre();
                $radio['dj'] = $app['service.shoutcast']->GetServerTitle();
            }
        }
        $data = array('users' => $users,
            'notices' => $notices,
            'error' => $app['security.last_error']($request),
            'radio' => $radio
        );
        return $app['twig']->render('index.html.twig', $data);
    }
}