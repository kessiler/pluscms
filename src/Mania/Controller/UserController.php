<?php
/**
 * Created by PhpStorm.
 * User: KESSILER
 * Date: 08/07/14
 * Time: 16:22
 */

namespace Mania\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UserController
 * @author Kessiler Rodrigues && Marcos Silva
 * @package Mania\Controller
 */
class UserController {

    public function logoutAction(Request $request, Application $app)
    {
        $app['session']->invalidate();
        return $app->redirect($app['url_generator']->generate('homepage'));
    }

    public function recoverAction(Request $request, Application $app)
    {
        if($request->isMethod('POST')) {
            $user = $app['repository.user']->findByMailAndPin($request->request->get('email'), $request->request->get('pin'));
            if ($user) {
                $hashBase = substr(md5(rand(0,999)), 0, 8);
                $hashURL = substr(base64_encode(md5($hashBase.$user[0]['id'] . $request->request->get('email') . $request->request->get('pin'))), 0, 50);
                $data = array(
                    'id' => $user[0]['id'],
                    'recoverHash' => $hashURL
                );
                $app['repository.user']->save($data);
                $mailer = \Swift_Message::newInstance();
                $app['mailer']->send($mailer
                    ->setSubject('Mania Hotel - Redefinir senha')
                    ->setFrom(array(SMTP_MAIL)) // replace with your own
                    ->setTo(array($user[0]['mail']))   // replace with email recipient
                    ->setBody($app['twig']->render('email.html.twig',   // email template
                        array('url'    => 'http://'. $request->getHttpHost().$request->getBaseUrl().$app['url_generator']->generate('recuperar').'/'.$hashURL,
                              'imgSrc' =>  $mailer->embed(\Swift_Image::fromPath($app['twig.loader.filesystem']->getPaths()[0].'/img/logo.png')))
                    ),'text/html'));
                $app['session']->getFlashBag()->add('message', array('type' => 'success', 'text' => 'Foi enviado um e-mail com as instruções para redefinição de senha.'));
                $redirect = $app['url_generator']->generate('recuperar');
                return $app->redirect($redirect);
            } else {
                $app['session']->getFlashBag()->add('message', array('type' => 'error', 'text' => 'Dados incorretos. Tente novamente'));
                $redirect = $app['url_generator']->generate('recuperar');
                return $app->redirect($redirect);
            }
        } else {
            $data = array(
                'viewForm' => true,
                'recoverStart' => true,
                'error' => $app['security.last_error']($request)
            );
            return $app['twig']->render('recover.html.twig', $data);
        }
    }

    public function recoverHashAction(Request $request, Application $app)
    {
        if($request->isMethod('POST')) {
            if($request->request->get('password') == $request->request->get('confirm_password')) {
                $data = array(
                    'id' => $request->request->get('id'),
                    'recoverHash' => null,
                    'password' => $app['security.encoder.digest']->encodePassword($request->request->get('password'), ''),
                );
                $app['repository.user']->save($data);
                $app['session']->getFlashBag()->add('message', array('type' => 'success', 'text' => 'A recuperação de senha foi finalizada com sucesso. Logue-se com sua nova senha!'));
                $data = array(
                    'viewForm' => false,
                    'recoverStart' => false,
                    'mail' => $request->request->get('email'),
                    'id'   => $request->request->get('id'),
                    'error' => $app['security.last_error']($request)
                );
            } else {
                $app['session']->getFlashBag()->add('message', array('type' => 'error', 'text' => 'A confirmação da senha deve coincidir com a senha digitada. Tente novamente.'));
                $data = array(
                    'viewForm' => true,
                    'recoverStart' => false,
                    'mail' => $request->request->get('email'),
                    'id'   => $request->request->get('id'),
                    'error' => $app['security.last_error']($request)
                );
            }
            return $app['twig']->render('recover.html.twig', $data);
        } else {
            $hashKey = $request->attributes->get('hashKey');
            if($hashKey) {
                $userData = $app['repository.user']->findBy('recoverHash', $hashKey);
                if($userData) {
                    $data = array(
                        'recoverStart' => false,
                        'viewForm' => true,
                        'id' => $userData[0]['id'],
                        'mail' => $userData[0]['mail'],
                        'error' => $app['security.last_error']($request)
                    );
                } else {
                    $app['session']->getFlashBag()->add('message', array('type' => 'error', 'text' => 'Link de recuperação inválido.'));
                    $data = array(
                        'recoverStart' => true,
                        'viewForm' => true,
                        'error' => $app['security.last_error']($request)
                    );
                }
            } else {
                $app['session']->getFlashBag()->add('message', array('type' => 'error', 'text' => 'Link de recuperação inválido.'));
                $data = array(
                    'recoverStart' => true,
                    'viewForm' => true,
                    'error' => $app['security.last_error']($request)
                );
            }
            return $app['twig']->render('recover.html.twig', $data);
        }
    }
} 