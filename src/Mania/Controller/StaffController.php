<?php
/**
 * Created by PhpStorm.
 * User: KESSILER
 * Date: 10/07/14
 * Time: 23:02
 */

namespace Mania\Controller;

use Mania\Repository\StaffRepository;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class StaffController
 * @author Kessiler Rodrigues && Marcos Silva
 * @package Mania\Controller
 */
class StaffController
{

    public function indexAction(Request $request, Application $app)
    {
        $founders = $app['repository.staff']->getStaffByType(StaffRepository::FOUNDER);
        $administrators = $app['repository.staff']->getStaffByType(StaffRepository::ADMIN);
        $managers = $app['repository.staff']->getStaffByType(StaffRepository::MANAGER);
        $moderators = $app['repository.staff']->getStaffByType(StaffRepository::MODERATOR);
        $helpers = $app['repository.staff']->getStaffByType(StaffRepository::HELPER);
        $data = array('error' => $app['security.last_error']($request),
            'founders' => $founders,
            'administrators' => $administrators,
            'managers' => $managers,
            'moderators' => $moderators,
            'helpers' => $helpers
        );
        return $app['twig']->render('equipe.html.twig', $data);
    }

} 