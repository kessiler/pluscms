<?php
/**
 * Created by PhpStorm.
 * User: KESSILER
 * Date: 11/07/14
 * Time: 21:00
 */

namespace Mania\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RegisterController
 * @author Kessiler Rodrigues && Marcos Silva
 * @package Mania\Controller
 */
class RegisterController
{

    public function indexAction(Request $request, Application $app)
    {
        if (BLOCK_LOGGED_RECORD && $app['security']->isGranted('ROLE_USER')) {
            $app['session']->getFlashBag()->add('message',
                array('type' => 'error', 'text' => 'Para realizar um novo cadastro, você não pode estar logado. <br> Deslogue-se e tente novamente.'));
            $data = array('formVisible' => false, 'error' => $app['security.last_error']($request));
        } else {
            $userReference = $request->request->get('username');
            if (!empty($userReference)) {
                $app['session']->set('userReference', $userReference);
            }
            $data = array('formVisible' => true, 'error' => $app['security.last_error']($request));
        }
        return $app['twig']->render('register.html.twig', $data);
    }

    public function registerAction(Request $request, Application $app)
    {
        if ($request->isMethod('POST')) {
            if ($this->validateName($request->request->get('username'))) {
                if ($app['repository.user']->findBy('username', $request->request->get('username'))) {
                    $app['session']->getFlashBag()->add('message', array('type' => 'error', 'text' => 'Nome do usuário informado já existe. Informe outro.'));
                } else {
                    if ($app['repository.user']->findBy('mail', $request->request->get('email'))) {
                        $app['session']->getFlashBag()->add('message', array('type' => 'error', 'text' => 'E-mail informado já existe. Informe outro.'));
                    } else {
                        $nameReference = $request->attributes->get('usuario') || $app['session']->get('userReference');
                        if ($nameReference) {
                            $user = $app['repository.user']->findBy('username', $nameReference);
                            if ($user) {
                                $referenceByIp = $app['repository.user']->getCountByIp($request->getClientIp());
                                if (!$referenceByIp) {
                                    $user['referencias'] = $user['referencias'] + 1;
                                    $user['vip_points'] = $user['vip_points'] + 1;
                                    $app['repository.user']->save($user);
                                    $app['session']->set('userReference', '');
                                }
                            }
                        }

                        $data = array(
                            'username' => $request->request->get('username'),
                            'password' => $app['security.encoder.digest']->encodePassword($request->request->get('password'), ''),
                            'mail' => $request->request->get('email'),
                            'ip_reg' => $request->getClientIp(),
                            'ip_last' => $request->getClientIp(),
                            'pin' => $request->request->get('pin'),
                            'account_created' => time(),
                            'ref_id' => isset($user) ? $user['id'] : ''
                        );
                        if ($app['repository.user']->save($data)) {
                            $app['session']->getFlashBag()->add('message', array('type' => 'success', 'text' => 'Cadastro realizado com sucesso.'));
                        }
                    }
                }
            } else {
                $app['session']->getFlashBag()->add('message', array('type' => 'error', 'text' => 'Nome de usuário inválido. Informe outro.'));
            }
            $redirect = $app['url_generator']->generate('cadastro');
            return $app->redirect($redirect);
        }
    }

    private function validateName($name)
    {
        if (preg_match('/^[a-zA-Z0-9.:,-@!=]+$/i', $name) && !preg_match('/mod-/i', $name)) {
            return true;
        } else {
            return false;
        }
    }

} 