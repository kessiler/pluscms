<?php
namespace Mania\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminNoticesController
 * @author Kessiler Rodrigues && Marcos Silva
 * @package Mania\Controller
 */
class AdminNoticesController
{

    public function indexAction(Request $request, Application $app)
    {
        $limit = 30;
        $total = $app['repository.notices']->getCount();
        $numPages = ceil($total / $limit);
        $currentPage = $request->query->get('page', 1);
        $offset = ($currentPage - 1) * $limit;
        $notices = $app['repository.notices']->findAll($limit, $offset);

        $data = array(
            'notices' => $notices,
            'currentPage' => $currentPage,
            'numPages' => $numPages,
            'here' => $app['url_generator']->generate('adminNotices'),
            'type' => 'view'
        );
        return $app['twig']->render('admin/noticias.html.twig', $data);
    }

    public function addAction(Request $request, Application $app)
    {
        if ($request->isMethod('POST')) {
            $data = array(
                'title' => $request->request->get('title'),
                'shortstory' => $request->request->get('description'),
                'image' => $request->request->get('image'),
                'author' => $app['security']->getToken()->getUser()->getId(),
                'longstory' => $request->request->get('content'),
            );
            if ($app['repository.notices']->save($data)) {
                $app['session']->getFlashBag()->add('message', array('type' => 'success', 'text' => 'Notícia cadastrada com sucesso.'));
                $redirect = $app['url_generator']->generate('adminNotices');
                return $app->redirect($redirect);
            } else {
                $app['session']->getFlashBag()->add('message', array('type' => 'error', 'text' => 'Ocorreu um erro ao tentar adicionar a notícia.'));
                $redirect = $app['url_generator']->generate('adminNotices');
                return $app->redirect($redirect);
            }
        }

        $data = array(
            'type' => 'add'
        );
        return $app['twig']->render('admin/noticias.html.twig', $data);
    }

    public function editAction(Request $request, Application $app)
    {
        $notice = $request->attributes->get('notice');
        if ($request->isMethod('POST')) {
            $data = array(
                'id' => $notice,
                'title' => $request->request->get('title'),
                'shortstory' => $request->request->get('description'),
                'image' => $request->request->get('image'),
                'author' => $app['security']->getToken()->getUser()->getId(),
                'longstory' => $request->request->get('content'),
            );
            if ($app['repository.notices']->save($data)) {
                $app['session']->getFlashBag()->add('message', array('type' => 'success', 'text' => 'Notícia editada com sucesso.'));
                $redirect = $app['url_generator']->generate('adminNotices');
                return $app->redirect($redirect);
            } else {
                $app['session']->getFlashBag()->add('message', array('type' => 'error', 'text' => 'Ocorreu um erro ao tentar editar a notícia.'));
                $redirect = $app['url_generator']->generate('adminNotices');
                return $app->redirect($redirect);
            }
        }

        $notice = $app['repository.notices']->find($notice);

        $data = array(
            'notice' => $notice,
            'type' => 'edit'
        );
        return $app['twig']->render('admin/noticias.html.twig', $data);
    }

    public function deleteAction(Request $request, Application $app)
    {
        $notice = $request->attributes->get('notice');
        $app['repository.notices']->delete($notice);
        return $app->redirect($app['url_generator']->generate('adminNotices'));
    }

} 